<?php

namespace App\Http\Controllers;

use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\Validator;
use Psy\Util\Json;

class BookController extends Controller
{
    public function index(): JsonResource
    {
        $books = Book::all();
        return BookResource::collection($books);
    }

    public function show($id)//: JsonResource
    {
        $book = Book::findOrFail($id);
        return new BookResource($book);
    }

    # JSON:Schema validation
    public function validation()
    {
        $inputJson = json_decode(file_get_contents(storage_path('app/jsonSchema/input.json')));

        $validator = new Validator();
        $validator->resolver()->registerFile(
            'http://api.example.com/schema.json',
            storage_path('app/jsonSchema/schema.json')
        );

        $result = $validator->validate($inputJson, file_get_contents(storage_path('app/jsonSchema/schema.json')));

        if ($result->isValid()) {
            return response()->json(["data" => ["validatiom" => "ok"]]);
        }

        $getErrors = (new ErrorFormatter())->format($result->error());
        $errors = array_pop($getErrors);
        return response()->json(["data" => $errors]);
        //return JsonResource::collection(array_pop($errors));
    }
}
