<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\Editorial;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [

            'name' => fake()->name(),
            'edition' => fake()->colorName(),
            'description' => fake()->paragraph(),
            'pages' => fake()->numberBetween(50,500),
            'isbn' => fake()->isbn13(),
            'author_id' => Author::factory(Author::class),
            'editorial_id' => Editorial::factory(Editorial::class)
        ];
    }
}
