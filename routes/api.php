<?php

use App\Http\Controllers\BookController;
use App\Http\Resources\BookCollection;
use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');


Route::get('book', [BookController::class, 'index']);
Route::get('book/{id}', [BookController::class, 'show']);
/*
Route::get('book', function() {
    return new BookCollection(Book::all()->keyBy->id);
});
*/

Route::get('book/validation', [BookController::class, 'validation']);
